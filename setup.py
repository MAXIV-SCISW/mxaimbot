from setuptools import setup

setup(
    name='mxaimbot',
    version='0.0.1',
    packages=['mxaimbot'],
    install_requires=[
        'keras',
        'tensorflow',
        'flask',
        'opencv-python',
        'h5py',
        'pillow',
        'pandas',
        'scikit-image',
        'tqdm',
    ],
    entry_points={
        'console_scripts': [
            'mxaimbot-eval=mxaimbot.evaluate:evaluate_all',
            'mxaimbot-server=mxaimbot.pred_server:main',
            'mxaimbot-train=mxaimbot.train:train',
            'mxaimbot-predict-dataset=mxaimbot.predict:predict_test_set',
            'mxaimbot-analysis-server=mxaimbot.analysis_server:run'
        ],
    }
)
