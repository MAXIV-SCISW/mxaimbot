import os
from pathlib import Path
from argparse import ArgumentParser
import tensorflow.keras as K
import numpy as np
from mxaimbot import core

def get_args():
    "Get commandline arguments with argparse"
    argp = ArgumentParser()
    argp.add_argument('dataset', help='path to dataset file (hdf5)', type=str)
    argp.add_argument('model', help='path to model file (hdf5)', type=str)
    argp.add_argument(
        '--run-label',
        help='label to append to the bboxes to differentiate from other run with the same model'
    )
    argp.add_argument(
        '--dry',
        action='store_true',
        help='don\'t write predictions to file, just print them.'
    )
    argp.add_argument(
        '--no-tta',
        action='store_true',
        help='don\'t use test-time augmentation with horizontal flipping.'
    )
    return argp.parse_args()


def predict_test_set():
    os.environ['HDF5_USE_FILE_LOCKING'] = 'FALSE'
    args = get_args()
    model: K.Model = K.models.load_model(args.model)
    print('model loaded')
    dataset = core.Dataset(args.dataset)
    names = dataset.list_frame_names(is_annotated=True)
    names.sort()
    # imsize = (128, 128, 1)
    imsize = model.input_shape[1:]
    print('imsize from model:', imsize)
    assert len(imsize) == 3
    assert imsize[0] == imsize[1] # because x == y
    # assert imsize[2] == 1 # because grayscale
    use_color = imsize[2] == 3
    batch_size = 64
    pred_steps = (len(names) // batch_size) + 1
    batch_gen = core.batch_generator(
        dataset,
        imsize,
        batch_size=batch_size,
        augment=True,
        aug_flip_p=0,
        aug_rot90_k=0,
        color=use_color,
    )
    if not args.no_tta:
        batch_gen_flipped = core.batch_generator(
            dataset,
            imsize,
            batch_size=batch_size,
            augment=True,
            aug_flip_p=1,
            aug_rot90_k=0,
            color=use_color,
        )
    print('predicting...')
    preds: np.ndarray = model.predict(
        batch_gen,
        use_multiprocessing=True,
        steps=pred_steps,
        verbose=1,
    )
    if not args.no_tta:
        print('predicting augmented...')
        preds_flipped: np.ndarray = model.predict(
            batch_gen_flipped,
            use_multiprocessing=True,
            steps=pred_steps,
            verbose=1,
        )
        preds = [
            core.merge_flipped_norm_bbox(real_bbox, flipped_bbox)
            for real_bbox, flipped_bbox
            in zip(preds, preds_flipped)
        ]
    if args.dry:
        print('dry run, printing predictions instead of saving:')
        for bbox in preds:
            print(', '.join([str(x) for x in bbox]))
    else:
        print('saving predictions...')
        for name, bbox in list(zip(names, preds))[:len(names)]:
            # print(name, bbox)
            dataset.add_prediction( # TODO Batch this shit up
                frame_name=name,
                model_name='{}{}'.format(
                    Path(args.model).stem,
                    ' [' + args.run_label + ']' if args.run_label else ''
                ),
                bbox=bbox,
            )
    print('done')

if __name__ == '__main__':
    predict_test_set()
