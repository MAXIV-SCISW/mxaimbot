from argparse import ArgumentParser
from pathlib import Path
import sys
import os
import tensorflow.keras as K
from mxaimbot import core, models, synthset

from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

config = ConfigProto()
config.gpu_options.allow_growth = True
session = InteractiveSession(config=config)

def get_args():
    "Get commandline arguments with argparse"
    argp = ArgumentParser()
    argp.add_argument('dataset', help='path to dataset file (hdf5)', type=str)
    argp.add_argument('--epochs', default=100, help='number of epochs to train for', type=int)
    argp.add_argument(
        '--size', '-s',
        default=128,
        type=int,
        help='side length of images fed to neural-nets in pixels',
    )
    argp.add_argument(
        '--out', '-o',
        help='where to write the trained model',
        )
    argp.add_argument(
        '--logdir', '-l',
        help='where to write the logs for tensorboard',
        )
    argp.add_argument(
        '--arch',
        help='name of the NN architecture to use',
        choices=list(models.ALL_ARCHES.keys()),
        )
    argp.add_argument(
        '--primed',
        help='path to a pre-trained model to use',
        )
    argp.add_argument(
        '--patience',
        help='patience for early stopping',
        type=int,
        )
    argp.add_argument('--color', action='store_true')
    return argp.parse_args()


def train():
    """experiment"""
    args = get_args()
    os.environ['CUDA_VISIBLE_DEVICES'] = '0,1'
    modelname = args.arch if args.arch else Path(args.primed).stem
    output_path = args.out or Path(os.environ.get('MXAIMBOT_MODEL_DIR', '.')) / \
            '{}_{}_s{}.h5'.format(modelname, Path(args.dataset).stem, args.size)
    log_dir = args.logdir or Path(os.environ.get('MXAIMBOT_TENSORBOARD_LOGS_DIR', None)) / \
            '{}_{}_s{}'.format(modelname, Path(args.dataset).stem, args.size)
    filepath = args.dataset
    imsize = (args.size, args.size, (3 if args.color else 1))
    print('training with imsize', imsize)
    if args.arch:
        if args.arch in models.ALL_ARCHES:
            model = models.ALL_ARCHES[args.arch](imsize=imsize)
        else:
            available_arches = '\n'.join(models.ALL_ARCHES.keys())
            print(
                f'Architecture "{args.arch}" is not available.\
                Available architectures are\n{available_arches}'
                , file=sys.stderr)
            sys.exit(1)
    elif args.primed:
        model = K.models.load_model(args.primed)
    else:
        print('You need to specify --arch or --primed', file=sys.stderr)
    bsize = 64

    if args.dataset == 'synth':
        train_gen = synthset.synth_batch_generator(batch_size=bsize)
        val_gen = synthset.synth_batch_generator(batch_size=bsize)
        steps_per_epoch = 100
        validation_steps = 20
    else:
        dset = core.Dataset(filepath)
        train_gen = core.batch_generator(
            dset,
            imsize,
            in_subset=core.Dataset.SET_TRAIN,
            batch_size=bsize,
            color=args.color,
            augment=True,
        )
        val_gen = core.batch_generator(
            dset,
            imsize,
            in_subset=core.Dataset.SET_VALID,
            batch_size=bsize,
            color=args.color,
            augment=True,
        )
        steps_per_epoch = len(dset.list_frame_names(in_subset=dset.SET_TRAIN)) // bsize
        validation_steps = len(dset.list_frame_names(in_subset=dset.SET_VALID)) // bsize

    tensorboard = K.callbacks.TensorBoard(log_dir=log_dir)
    model.fit(
        train_gen,
        epochs=args.epochs,
        validation_data=val_gen,
        steps_per_epoch=steps_per_epoch,
        validation_steps=validation_steps,
        callbacks=[
            K.callbacks.EarlyStopping(
                monitor='val_loss',
                patience=args.patience,
                restore_best_weights=True
                ),
            tensorboard,
            K.callbacks.ModelCheckpoint(
                str(output_path),
                save_best_only=True,
                mode='min',
                verbose=1
            ),
            ],
    )
    model.save(str(output_path))
    print('done')


if __name__ == '__main__':
    train()
