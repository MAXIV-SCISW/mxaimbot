from pathlib import Path
import typing as T
import math
import random
import cv2
import numpy as np
from tqdm import tqdm
from skimage.segmentation import random_walker
from skimage.transform import resize
from skimage.measure import block_reduce


def segment_image(img):
    markers = np.zeros_like(img, dtype=np.uint8)
    rows, cols = img.shape[:2]
    # TODO set marker seeds
    # print(img.max())
    # print(img.mean())
    # print(img.min())
    markers[img < 80] = 2
    markers[img > 220] = 1
    markers[:, 0] = 1
    markers[:, -1] = 1
    segmentation = random_walker(img, markers)
    return markers, segmentation


def point_on_background(mask: np.ndarray, ransac_candidates=100, background_value=0, patience=1000) -> (int, int):
    smallmask = block_reduce(mask, (4,4), func=np.max)
    candidates = set()
    while len(candidates) < ransac_candidates:
        row, col = (random.randint(0, smallmask.shape[0]-1), random.randint(0, smallmask.shape[1]-1))
        if smallmask[row, col] == background_value:
            candidates.add((row, col))
        else:
            patience -= 1
    candidates = list(candidates)

    seed_point = None
    biggest_area = 0
    for row, col in candidates:
        fill = cv2.copyTo(smallmask, None)
        cv2.floodFill(fill, None, (col, row), 128)
        area = ((fill == 128) * 1).sum()
        if area > biggest_area:
            biggest_area = area
            seed_point = (row, col)
    size_factor = mask.shape[0] // smallmask.shape[0]
    ret = (seed_point[1] * size_factor, seed_point[0] * size_factor) # x, y
    if not mask[ret[1], ret[0]] == background_value:
        print("ERROR")
        breakpoint()

    return ret



def bg_mask(img: np.ndarray) -> np.ndarray:
    """
    :param img: grayscale image
    returns a 128x128 mask where background is False and foreground is True.
    """
    edges: np.ndarray = cv2.Canny(img, 100, 140)
    kernel = np.ones((20, 20)) * 255
    edges = cv2.dilate(edges, kernel)
    edges = cv2.erode(edges, kernel)
    flood_orig = point_on_background(edges)
    cv2.floodFill(edges, None, flood_orig, 128)
    mask = edges == 128
    return mask


def flatten_background(img):
    mask = bg_mask(img)
    return np.clip(img + mask*255, 0, 255)


def mask_out_bg(img, debug=False):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gray = cv2.resize(gray, (128, 128))
    edges: np.ndarray = cv2.Canny(gray, 100, 140)
    kernel = np.ones((3, 3)) * 255
    dilated = cv2.dilate(edges, kernel, iterations=6)
    eroded = cv2.erode(dilated, kernel, iterations=6)
    fill = eroded.copy()
    flood_orig = point_on_background(fill)
    cv2.floodFill(fill, None, (0, fill.shape[0]-1), 128)
    cv2.floodFill(fill, None, (fill.shape[1]-1, fill.shape[0]-1), 128)
    mask = fill != 128
    if debug:
        steps = np.concatenate([gray, edges, dilated, eroded, mask*255])
        return mask, steps
    else:
        return mask, None


def get_section(img, coords, rad):
    """
    >>> get_center_section(np.array([[ 1,  2,  3,  4], \
                                     [ 5,  6,  7,  8], \
                                     [ 9, 10, 11, 12], \
                                     [13, 14, 15, 16]]), rad=1)
    (1, 2, 1, 2)
    """
    assert rad > 0
    rows, cols = img.shape
    center_row = coords[1]
    center_col = coords[0]
    return (
        math.floor(center_row)-(rad-1),
        math.ceil(center_row)+(rad-1),
        math.floor(center_col)-(rad-1),
        math.ceil(center_col)+(rad-1)
    )


def sanity_check(img: np.ndarray, point: tuple, margin: int) -> (bool, np.ndarray):
    """
    Check if the center of an image is centered on anything other than the background.

    param img: the image to be checked
    param point: x, y centering location who's sanity is to be determined.
    param margin: minimum distance from any edge in x or y direction.

    returns
        a bool which is true iff the check passed
        a mask of the center of the segmentation.
            If all elements of the mask is true, the check is passed.
            The size of the mask is determined by the margin parameter.


    """
    mask, _ = mask_out_bg(img)
    cr0, cr1, cc0, cc1 = get_section(mask, point, margin)
    center = mask[cr0:cr1, cc0:cc1] > 0
    return bool(center.all()), mask


def debug_sanity_check():
    imdir = Path('/home/isak/datasets/mxaimbot/mrcnn/imgs')
    for path in tqdm(list(imdir.glob('*.jpg'))):
        img = cv2.imread(str(path), cv2.IMREAD_COLOR)
        mask, steps = mask_out_bg(img)

        cr0, cr1, cc0, cc1 = get_section(mask, (64, 64), 3)
        center = mask[cr0:cr1, cc0:cc1]
        start_of_final_step = steps.shape[0] - mask.shape[0]
        steps[start_of_final_step+cr0:start_of_final_step+cr1, cc0:cc1] = 128
        steps = steps.astype('uint8')
        center_representation = (center > 0) * 1
        center_representation = ''.join([str(row) for row in center_representation])
        steps = cv2.putText(
            steps,
            str(center_representation),
            (0, steps.shape[0]-10),
            cv2.FONT_HERSHEY_SIMPLEX,
            .25,
            255
        )
        dstdir = Path('./contours')
        dstdir.mkdir(exist_ok=True)
        cv2.imwrite(str(dstdir / path.name), steps)

def debug_flatten():
    imdir = Path('/home/isak/datasets/mxaimbot/test_images')
    dstdir = Path('/tmp/flats')
    dstdir.mkdir(exist_ok=True)
    for path in tqdm(list(imdir.glob('*.jpg'))):
        img = cv2.imread(str(path), cv2.IMREAD_COLOR)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img = cv2.resize(img, (128, 128))
        img = flatten_background(img)
        cv2.imwrite(str(dstdir / path.name))



def debug_sane_xcenter():
    imdir = Path('/home/isak/datasets/mxaimbot/test_images')
    dstdir = Path('./centerlines')
    dstdir.mkdir(exist_ok=True)
    for path in tqdm(list(imdir.glob('*.jpg'))):
        img = cv2.imread(str(path), cv2.IMREAD_COLOR)
        mask, _ = mask_out_bg(img)
        scale_up_factor_y = img.shape[0] / mask.shape[0]
        scale_up_factor_x = img.shape[1] / mask.shape[1]
        for y in range(mask.shape[0]):
            x = sane_xcenter_from_mask(mask, y)
            if x is not None:
                fsy = int(y * scale_up_factor_y)
                fsx = int(x * scale_up_factor_x)
                img[fsy-5:fsy+5, fsx-5:fsx+5] = (0, 0, 255)
            else:
                break
        cv2.imwrite(str(dstdir / path.name), img)


def left_edge(row):
    for idx, val in enumerate(row):
        if val:
            return idx
    return None

def right_edge(row):
    for idx, val in list(enumerate(row))[::-1]:
        if val:
            return idx
    return None

def sane_xcenter_from_mask(mask, y):
    row = mask[int(y), :]
    l_edge, r_edge = left_edge(row), right_edge(row)
    if l_edge is None or r_edge is None:
        return None
    x = (l_edge + r_edge)//2
    return x

def sane_xcenter_from_img(img, y):
    mask, _ = mask_out_bg(img)
    scale_up_factor_y = img.shape[0] / mask.shape[0]
    scale_up_factor_x = img.shape[1] / mask.shape[1]
    mask_y = int(y // scale_up_factor_y)
    mask_x = sane_xcenter_from_mask(mask, mask_y)
    if mask_x is None:
        return None
    x = int(mask_x * scale_up_factor_x)
    return x

if __name__ == '__main__':
    debug_sanity_check()
    # debug_sane_xcenter()

