import tensorflow.keras as K
import tensorflow.keras.layers as L
import typing as T


def dense_stairs(start_neurons, end_neurons, num_layers):
    layers = []
    neurons = start_neurons
    neuron_delta = (end_neurons - start_neurons) // layers
    for _ in range(num_layers):
        layers.append(K.layers.Dense(neurons))
        neurons += neuron_delta
    return layers


def conv_stairs(
        start_filters: int,
        end_filters: int,
        num_layers: int,
        kernel_size: T.Union[int, T.Tuple[int, int]],
        layer_args: dict = None,
        activation: T.Callable = None,
        ) -> T.List[K.layers.Layer]:
    """
    returns a list of `num_layers` Conv2D layers whos number of filters ascend/descend linearly
    from `start_filters` to `end_filters`.
    `layer_args` is a dict of parameters that will be sent to each layer.
    `activation` will be added to the list after each layer. If ommitted, LeakyReLU is used.
    """
    layers = []
    delta = (end_filters - start_filters) // layers
    for i in range(num_layers):
        layers.append(K.layers.Conv2D(start_filters + delta * i, kernel_size, **(layer_args or dict())))
        layers.append(activation or L.LeakyReLU())
    return layers


def gen_model(name, imsize):
    md = K.Sequential(
        [
            L.BatchNormalization(input_shape=imsize),
            conv_stairs(16, 64, 3, kernel_size=3),
            L.MaxPool2D(pool_size=2),
        ])


def get_model_mid(imsize) -> K.Model:
    md = K.Sequential(
        [
            L.BatchNormalization(input_shape=imsize),

            L.Conv2D(filters=16, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=32, kernel_size=3), L.LeakyReLU(),
            L.MaxPool2D(pool_size=2),

            L.Conv2D(filters=64, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=128, kernel_size=3), L.LeakyReLU(),
            L.MaxPool2D(pool_size=3),

            L.Flatten(),

            L.Dense(128), L.LeakyReLU(),
            L.Dense(64), L.LeakyReLU(),
            L.Dense(32), L.LeakyReLU(),
            L.Dense(4, activation='linear'),
        ], name="mid2",
    )
    md.compile(
        optimizer='adam',
        loss=K.losses.mean_squared_error,
    )
    return md

def get_model_mid2(imsize) -> K.Model:
    """
    adding batch normalization to mid model
    """
    md = K.Sequential(
        [
            L.BatchNormalization(input_shape=imsize),
            L.Conv2D(filters=16, kernel_size=3), L.LeakyReLU(),
            L.BatchNormalization(),
            L.Conv2D(filters=32, kernel_size=3), L.LeakyReLU(),
            L.BatchNormalization(),
            L.MaxPool2D(pool_size=2),

            L.Conv2D(filters=64, kernel_size=3), L.LeakyReLU(),
            L.BatchNormalization(),
            L.Conv2D(filters=128, kernel_size=3), L.LeakyReLU(),
            L.BatchNormalization(),
            L.MaxPool2D(pool_size=2),

            L.Flatten(),

            L.Dense(128), L.LeakyReLU(),
            L.BatchNormalization(),
            L.Dense(64), L.LeakyReLU(),
            L.BatchNormalization(),
            L.Dense(32), L.LeakyReLU(),
            L.BatchNormalization(),
            L.Dense(4, activation='linear'),
        ], name="mid2",
    )
    md.compile(
        optimizer='adam',
        loss=K.losses.mean_squared_error,
    )
    return md


def get_model_mid3(imsize) -> K.Model:
    """
    adding batch normalization to mid model
    """
    md = K.Sequential(
        [
            L.BatchNormalization(input_shape=imsize),

            L.Conv2D(filters=16, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=32, kernel_size=3), L.LeakyReLU(),
            L.BatchNormalization(),
            L.MaxPool2D(pool_size=2),

            L.Conv2D(filters=64, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=128, kernel_size=3), L.LeakyReLU(),
            L.BatchNormalization(),
            L.MaxPool2D(pool_size=2),

            L.Flatten(),

            L.Dense(128), L.LeakyReLU(),
            L.Dense(64), L.LeakyReLU(),
            L.Dense(32), L.LeakyReLU(),
            L.Dense(16), L.LeakyReLU(),
            L.Dense(4, activation='linear'),
        ], name="mid3",
    )
    md.compile(
        optimizer='adam',
        loss=K.losses.mean_squared_error,
    )
    return md

def get_model_long1drop(imsize) -> K.Model:
    md = K.Sequential(
        [
            L.BatchNormalization(input_shape=imsize),

            L.Conv2D(filters=8, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=16, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=32, kernel_size=3), L.LeakyReLU(),
            L.MaxPool2D(pool_size=2),

            L.Conv2D(filters=64, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=128, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=256, kernel_size=3), L.LeakyReLU(),
            L.MaxPool2D(pool_size=3),

            L.Flatten(),

            L.Dense(256), L.LeakyReLU(),
            L.Dropout(0.1),
            L.Dense(128), L.LeakyReLU(),
            L.Dropout(0.1),
            L.Dense(64), L.LeakyReLU(),
            L.Dropout(0.1),
            L.Dense(32), L.LeakyReLU(),
            L.Dropout(0.1),
            L.Dense(4, activation='linear'),
        ], name="long1drop"
    )
    md.compile(
        optimizer='adam',
        loss=K.losses.mean_squared_error,
    )
    return md


def get_model_long1(imsize) -> K.Model:
    md = K.Sequential(
        [
            L.BatchNormalization(input_shape=imsize),

            L.Conv2D(filters=8, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=16, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=32, kernel_size=3), L.LeakyReLU(),
            L.MaxPool2D(pool_size=2),

            L.Conv2D(filters=64, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=128, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=256, kernel_size=3), L.LeakyReLU(),
            L.MaxPool2D(pool_size=3),

            L.Flatten(),

            L.Dense(256), L.LeakyReLU(),
            L.Dense(128), L.LeakyReLU(),
            L.Dense(64), L.LeakyReLU(),
            L.Dense(32), L.LeakyReLU(),
            L.Dense(4, activation='linear'),
        ], name="long1"
    )
    md.compile(
        optimizer='adam',
        loss=K.losses.mean_squared_error,
    )
    return md


def get_model_long1reg1(imsize) -> K.Model:
    md = K.Sequential(
        [
            L.BatchNormalization(input_shape=imsize),

            L.Conv2D(filters=8, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=16, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=32, kernel_size=3), L.LeakyReLU(),
            L.MaxPool2D(pool_size=2),

            L.Conv2D(filters=64, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=128, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=256, kernel_size=3), L.LeakyReLU(),
            L.MaxPool2D(pool_size=3),

            L.Flatten(),

            L.Dense(256), L.LeakyReLU(), L.Dropout(.25), L.BatchNormalization(),
            L.Dense(128), L.LeakyReLU(), L.Dropout(.25), L.BatchNormalization(),
            L.Dense(64), L.LeakyReLU(), L.Dropout(.25), L.BatchNormalization(),
            L.Dense(32), L.LeakyReLU(), L.Dropout(.25), L.BatchNormalization(),
            L.Dense(4, activation='linear'),
        ], name="long1"
    )
    md.compile(
        optimizer='adam',
        loss=K.losses.mean_squared_error,
    )
    return md

def get_model_long3(imsize) -> K.Model:
    md = K.Sequential(
        [
            L.BatchNormalization(input_shape=imsize),

            L.Conv2D(filters=8, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=16, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=32, kernel_size=3), L.LeakyReLU(),

            L.Conv2D(filters=44, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=56, kernel_size=3), L.LeakyReLU(),
            L.MaxPool2D(pool_size=2),

            L.Conv2D(filters=64, kernel_size=5), L.LeakyReLU(),
            L.Conv2D(filters=128, kernel_size=5), L.LeakyReLU(),
            L.Conv2D(filters=256, kernel_size=5), L.LeakyReLU(),
            L.Conv2D(filters=512, kernel_size=5), L.LeakyReLU(),
            L.MaxPool2D(pool_size=3),

            L.Flatten(),

            L.Dense(512), L.LeakyReLU(),
            L.Dense(256), L.LeakyReLU(),
            L.Dense(128), L.LeakyReLU(),
            L.Dense(64), L.LeakyReLU(),
            L.Dense(32), L.LeakyReLU(),
            L.Dense(4, activation='linear'),
        ], name="long3"
    )
    md.compile(
        optimizer='adam',
        loss=K.losses.mean_squared_error,
    )
    return md


def get_model_long4(imsize) -> K.Model:
    md = K.Sequential(
        [
            L.BatchNormalization(input_shape=imsize),

            L.Conv2D(filters=16, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=16, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=32, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=32, kernel_size=3), L.LeakyReLU(),
            L.BatchNormalization(),

            L.Conv2D(filters=64, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=64, kernel_size=3), L.LeakyReLU(),
            L.BatchNormalization(),

            L.Conv2D(filters=128, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=128, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=256, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=256, kernel_size=3), L.LeakyReLU(),
            L.BatchNormalization(),

            L.Flatten(),

            L.Dense(256), L.LeakyReLU(),
            L.Dropout(.25),
            L.Dense(128), L.LeakyReLU(),
            L.Dense(64), L.LeakyReLU(),
            L.Dense(32), L.LeakyReLU(),
            L.Dropout(.25),
            L.Dense(16), L.LeakyReLU(),
            L.Dense(8), L.LeakyReLU(),
            L.Dense(4, activation='linear'),
        ], name="long4"
    )
    md.compile(
        optimizer='adam',
        loss=K.losses.mean_squared_error,
    )
    return md

def get_model_long3norm3(imsize) -> K.Model:
    md = K.Sequential(
        [
            L.BatchNormalization(input_shape=imsize),

            L.Conv2D(filters=8, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=16, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=32, kernel_size=3), L.LeakyReLU(),
            L.BatchNormalization(),

            L.Conv2D(filters=44, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=56, kernel_size=3), L.LeakyReLU(),
            L.BatchNormalization(),
            L.MaxPool2D(pool_size=2),

            L.Conv2D(filters=64, kernel_size=5), L.LeakyReLU(),
            L.Conv2D(filters=128, kernel_size=5), L.LeakyReLU(),
            L.Conv2D(filters=256, kernel_size=5), L.LeakyReLU(),
            L.Conv2D(filters=512, kernel_size=5), L.LeakyReLU(),
            L.BatchNormalization(),
            L.MaxPool2D(pool_size=3),

            L.Flatten(),

            L.Dense(512), L.LeakyReLU(),
            L.Dense(256), L.LeakyReLU(),
            L.Dense(128), L.LeakyReLU(),
            L.Dense(64), L.LeakyReLU(),
            L.Dense(32), L.LeakyReLU(),
            L.Dense(4, activation='linear'),
        ], name="long3norm3")
    md.compile(
        optimizer='adam',
        loss=K.losses.mean_squared_error,
    )
    return md

def get_model_c4d4_b8t64(imsize) -> K.Model:
    md = K.Sequential(
        [
            L.BatchNormalization(input_shape=imsize),

            L.Conv2D(filters=8, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=16, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=32, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=64, kernel_size=3), L.LeakyReLU(),

            L.Flatten(),

            L.Dense(64), L.LeakyReLU(),
            L.Dense(32), L.LeakyReLU(),
            L.Dense(16), L.LeakyReLU(),
            L.Dense(8), L.LeakyReLU(),

            L.Dense(4, activation='linear'),
        ], name="c4d4b8t64"
    )
    md.compile(
        optimizer='adam',
        loss=K.losses.mean_squared_error,
    )
    return md


def get_model_longthin1(imsize) -> K.Model:
    md = K.Sequential(
        [
            L.BatchNormalization(input_shape=imsize),

            L.Conv2D(filters=10, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=20, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=30, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=40, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=50, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=60, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=70, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=80, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=90, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=100, kernel_size=3), L.LeakyReLU(),

            L.Flatten(),

            L.Dense(100), L.LeakyReLU(),
            L.Dense(50), L.LeakyReLU(),
            L.Dense(25), L.LeakyReLU(),
            L.Dense(4, activation='linear'),
        ], name="longthin1"
    )
    md.compile(
        optimizer='adam',
        loss=K.losses.mean_squared_error,
    )
    return md


def get_model_long2(imsize) -> K.Model:
    md = K.Sequential(
        [
            L.BatchNormalization(input_shape=imsize),

            L.Conv2D(filters=8, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=16, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=32, kernel_size=3), L.LeakyReLU(),

            L.MaxPool2D(pool_size=2),

            L.Conv2D(filters=64, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=128, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=256, kernel_size=3), L.LeakyReLU(),

            L.MaxPool2D(pool_size=3),

            L.Conv2D(filters=512, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=1024, kernel_size=3), L.LeakyReLU(),

            L.Flatten(),

            L.Dense(128), L.LeakyReLU(),
            L.Dense(64), L.LeakyReLU(),
            L.Dense(32), L.LeakyReLU(),
            L.Dense(4, activation='linear'),

        ], name="long2"
    )
    md.compile(
        optimizer='adam',
        loss=K.losses.mean_squared_error,
    )
    return md


def get_model_long1nodense(imsize) -> K.Model:
    md = K.Sequential(
        [
            L.BatchNormalization(input_shape=imsize),

            L.Conv2D(filters=8, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=16, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=32, kernel_size=3), L.LeakyReLU(),
            L.MaxPool2D(pool_size=2),

            L.Conv2D(filters=64, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=128, kernel_size=3), L.LeakyReLU(),
            L.Conv2D(filters=256, kernel_size=3), L.LeakyReLU(),
            L.MaxPool2D(pool_size=3),

            L.Flatten(),

            L.Dense(4, activation='linear'),
        ], name="long1nodense"
    )
    md.compile(
        optimizer='adam',
        loss=K.losses.mean_squared_error,
    )
    return md

def get_model_small(imsize) -> K.Model:
    md = K.Sequential(
        [
            L.BatchNormalization(input_shape=imsize),
            L.Conv2D(filters=32, kernel_size=3, activation='relu'),
            L.Conv2D(filters=64, kernel_size=3, activation='relu'),
            L.Flatten(),
            L.Dense(4, activation='linear'),
        ], name='small'
    )
    md.compile(
        optimizer='adam',
        loss=K.losses.mean_squared_error,
    )
    return md

ALL_ARCHES = dict(
    small=get_model_small,
    mid=get_model_mid,
    mid2=get_model_mid2,
    mid3=get_model_mid3,
    long1=get_model_long1,
    long1drop=get_model_long1drop,
    long1reg1=get_model_long1reg1,
    long1nodense=get_model_long1nodense,
    long2=get_model_long2,
    long3=get_model_long3,
    long3norm3=get_model_long3norm3,
    long4=get_model_long4,
    longthin1=get_model_longthin1,
    c4d4_b8t64=get_model_c4d4_b8t64,
)
