from os.path import isfile
from typing import Tuple, Union, Collection, List
from h5py import File, Group, Dataset
from PIL import Image
import numpy as np


class H5Database:
    """
    Interfaces with an HDF5 file with the following structure:
    {frames}/
        {frame_name_000}/
            img (dataset)
            annotated_bbox
            pred/
                {model_name} (bbox)
        {frame_name_001}/ ...
        {frame_name_002}/ ...
        ...
    """

    SET_TRAIN = 0b0001
    SET_VALID = 0b0010
    SET_TEST = 0b0100
    SET_SHIT = 0b1000

    NAME_FRAMES_BASE = 'frames'
    NAME_ATTR_SUBSET = 'subset'
    NAME_DSET_IMAGE = 'img'
    NAME_ANNOTATION_BOUNDING_BOX = 'annotated_bounding_box'

    def __init__(self, filepath: str, base_path=None):
        self.filepath = filepath
        self.base_path = base_path or self.NAME_FRAMES_BASE

    def add_frame(self, img: np.ndarray, zoom=None, name=None) -> str:
        """
        Adds a frame with the given image.
        Returns the name of the new frame.
        """
        with File(self.filepath, 'a') as fh:
            frames_grp = fh.require_group(self.NAME_FRAMES_BASE)
            frame_name = name or 'FRAME_{}'.format(str(len(frames_grp)).zfill(8))
            frame = frames_grp.create_group(frame_name)
            frame.create_dataset('img', data=img, compression='gzip')
            if zoom:
                frame.attrs.create('zoom', zoom)
            return frame_name

    def add_frames(self, gen):
        with File(self.filepath, 'a') as fh:
            frames_grp = fh.require_group(self.NAME_FRAMES_BASE)
            for data in gen:
                img = data['img']
                zoom = data.get('zoom')
                name = data.get('name')
                time = data.get('time')
                anno = data.get('anno')
                frame_name = name or 'FRAME_{}'.format(str(len(frames_grp)).zfill(8))
                frame = frames_grp.create_group(frame_name)
                img_ds = frame.create_dataset('img', data=img, compression='gzip')
                if zoom is not None:
                    frame.attrs.create('zoom', zoom)
                if time is not None:
                    img_ds.attrs.create('time', time)
                if anno is not None:
                    self.add_annotation(frame_name, anno, fh)

    def list_frame_names(self, is_annotated: bool = None, include_bad: bool = False,
                         sort_by_time: bool = False, in_subset: Union[int, Collection[int]] = None):
        with File(self.filepath, 'r') as fileroot:
            base = fileroot[self.base_path]
            children = list(base)
            # if sort_by_time:
            #     children.sort(key=lambda p: base[p][self.NAME_DSET_IMAGE].attrs.get('time'))

            if in_subset is not None:
                if isinstance(in_subset, int):
                    in_subset = [in_subset]
                children = [
                    c for c in children
                    if base[c].attrs.get(self.NAME_ATTR_SUBSET) in in_subset
                ]
            if not include_bad:
                children = [
                    c for c in children
                    if base[c].attrs.get(self.NAME_ATTR_SUBSET, default=0) != self.SET_SHIT
                ]
            if is_annotated is not None:
                children = [
                    c for c in children
                    if is_annotated == (self.NAME_ANNOTATION_BOUNDING_BOX in list(base[c]))
                ]
            return children


    def get_frame_images(self, frame_names: Collection[str]) -> List[np.ndarray]:
        with File(self.filepath, 'r') as fileroot:
            base = fileroot[self.base_path]
            ret = [base[frame_name][self.NAME_DSET_IMAGE][:] for frame_name in frame_names]
            return ret

    def get_frame_image(self, frame_name: str) -> np.ndarray:
        return self.get_frame_images([frame_name])[0]

    def add_annotation(
            self,
            frame_name: str,
            bbox: Tuple[float, float, float, float],
            fileroot,
            annotation_name=None,
        ) -> None:
        """
        Add an annotation bounding box `bbox` to a frame with name `frame_name`.
        if there already is an annotation with the specified (or default) annotation name,
        it will be overwritten.
        """
        annotation_name = annotation_name or self.NAME_ANNOTATION_BOUNDING_BOX
        frame = fileroot[self.base_path][frame_name]
        if annotation_name in list(frame):
            del frame[annotation_name]
        frame.create_dataset(annotation_name, data=bbox)

    def get_annotations(
            self,
            frame_names: List[str],
            annotation_name=None
        ) -> List[Tuple[float, float, float, float]]:
        """
        Get the annotation of the multiple given frames.
        """
        annotation_name = annotation_name or self.NAME_ANNOTATION_BOUNDING_BOX
        with File(self.filepath, 'r') as fileroot:
            return [
                tuple(fileroot[self.base_path][frame_name][annotation_name][:])
                for frame_name in frame_names
                ]

    def get_annotation(
            self,
            frame_name: str,
            annotation_name=None
        ) -> Tuple[float, float, float, float]:
        """
        Get the annotation of the given frame.
        """
        return self.get_annotations([frame_name], annotation_name)[0]

    def add_prediction(
            self,
            frame_name: str,
            model_name: str,
            bbox: Tuple[float, float, float, float]
        ):
        with File(self.filepath, 'r+') as fileroot:
            frame = fileroot[self.base_path][frame_name]
            frame.require_group('pred').create_dataset(model_name, data=bbox)

    def get_predictions(self, frame_name: str) -> List[Tuple[str, np.ndarray]]:
        with File(self.filepath, 'r') as fileroot:
            frame = fileroot[self.base_path][frame_name]
            preds = frame.require_group('pred')
            return [(p, preds[p][:]) for p in list(preds)]

    def get_worst_predictions(self, count, predictor_name):
        names = self.list_frame_names(is_annotated=True, in_subset=self.SET_TEST)
        annos = self.get_annotations(names)
        with File(self.filepath, 'r') as fileroot:
            base = fileroot[self.base_path]
            scoreboard = [] # name, distance
            bar = 0
            for name, anno in zip(names, annos):
                pred_bbox = base[name].require_group('pred')[predictor_name]
                dist = (pred_bbox[0] - anno[0])**2 + (pred_bbox[1] - anno[1])**2
                if len(scoreboard) < count or dist > bar:
                    scoreboard.append((name, dist))
                    scoreboard.sort(key=lambda x: x[1], reverse=True)
                    if len(scoreboard) > count:
                        scoreboard = scoreboard[:count]
            return scoreboard

    def set_frame_attr(self, frame_name: str, attr_key: str, value):
        with File(self.filepath, 'r+') as fileroot:
            fileroot[self.base_path][frame_name].attrs.create(attr_key, value)

    def assign_to_subset(self, frame_name: str, subset: int):
        assert isinstance(subset, int)
        self.set_frame_attr(frame_name, self.NAME_ATTR_SUBSET, subset)

    def get_last_absorbed(self) -> str:
        with File(self.filepath, 'r') as fileroot:
            return fileroot.require_group('meta').attrs.get('last_absorbed', default=None)

    def set_last_absorbed(self, uuid) -> str:
        with File(self.filepath, 'r') as fileroot:
            return fileroot.require_group('meta').attrs.create('last_absorbed', uuid)


