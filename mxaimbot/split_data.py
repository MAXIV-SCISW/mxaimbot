"""
Assigns each annotated frame in the database to the test-, train-, or validation-set.
The frames are not shuffled or assigned randomly.
The train set will consist of the first (in time) block of frames, then validation, and so on.
This is to avoid overfitting, as otherwise images in the test set might have similar images in the
test set.
"""
import argparse
from mxaimbot.h5db import H5Database


def run():
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    parser.add_argument('--train', type=float, default=0.6)
    parser.add_argument('--validation', type=float, default=0.2)
    parser.add_argument('--test', type=float, default=0.2)
    args = parser.parse_args()

    # define ratios

    db = H5Database(args.file)
    frames = db.list_frame_names(is_annotated=True, sort_by_time=False)

    ratios = [args.train, args.validation, args.test]
    print(ratios)
    totals = [int(r * len(frames)) for r in ratios]
    N_train, N_val, N_test = totals

    train = frames[:N_train]
    val = frames[N_train:N_train+N_val]
    test = frames[N_train+N_val:]

    for name in train:
        print(f"assigning {name} to training set")
        db.assign_to_subset(name, db.SET_TRAIN)

    for name in val:
        print(f"assigning {name} to validation set")
        db.assign_to_subset(name, db.SET_VALID)

    for name in test:
        print(f"assigning {name} to test set")
        db.assign_to_subset(name, db.SET_TEST)

if __name__ == '__main__':
    run()
