import argparse
import random
import io
import flask
import cv2
import numpy as np
import requests
from mxaimbot.h5db import H5Database
import mxaimbot.util as util
from mxaimbot.sanity_check import mask_out_bg
from mxaimbot import core

parser = argparse.ArgumentParser()
parser.add_argument('--dataset')
parser.add_argument('--port', type=int, default=7887)
parser.add_argument('--model', type=str)
args = parser.parse_args()
app = flask.Flask(__name__)
if 'dataset' in args:
    db = H5Database(args.dataset)


def image_response(img):
    _, imbytes = cv2.imencode('.jpeg', img)
    resbytes = io.BytesIO(imbytes)
    return flask.Response(resbytes, mimetype='image/jpeg')


@app.route('/img/<frame_name>')
def get_frame(frame_name):
    raw_img = db.get_frame_image(frame_name)
    raw_anno = db.get_annotation(frame_name)
    anno = util.scale_abnormalize_xywh(raw_anno, raw_img.shape[:2])
    img = cv2.circle(raw_img, anno[:2], 10, (0, 255, 0), 5)
    # xyxy = util.xywh_to_xyxy(anno)
    # img = cv2.rectangle(img, xyxy[:2], xyxy[2:], (0, 255, 0), 3)
    preds = db.get_predictions(frame_name)
    if 'model' in args:
        preds = [(name, box) for name, box in preds if name == args.model]
    for predname, raw_pred_bbox in preds:
        pred_bbox = util.scale_abnormalize_xywh(raw_pred_bbox, img.shape[:2])
        img = cv2.circle(img, pred_bbox[:2], 10, (255, 25, 25), 5)
        # p_xyxy = util.xywh_to_xyxy(pred_bbox)
        # img = cv2.rectangle(img, p_xyxy[:2], p_xyxy[2:], (255, 0, 0), 3)
        img = cv2.putText(
            img,
            predname,
            (pred_bbox[0] + 12, pred_bbox[1]),
            cv2.FONT_HERSHEY_SIMPLEX,
            .5,
            (255, 25, 25)
        )
    img = cv2.putText(img, frame_name, (10, 10), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255))
    # test with predserver
    # _, rawimbytes = cv2.imencode('.jpeg', raw_img)
    # rawimiobytes = io.BytesIO(rawimbytes)
    # predres = requests.post('http://localhost:7777/predict', data=rawimiobytes)
    # serverpred_bbox = predres.json()['bbox']
    # img = cv2.circle(img, tuple(serverpred_bbox[:2]), 10, (0, 0, 255), 3)
    return image_response(img)

@app.route('/mask/<frame_name>')
def get_mask(frame_name):
    raw_img = db.get_frame_image(frame_name)
    mask, _ = mask_out_bg(raw_img)
    mask = cv2.resize(mask, (raw_img.shape[:1:-1]), interpolation=cv2.INTER_NEAREST)
    masked = raw_img * (mask > 0)
    return image_response(masked)


@app.route('/info/<frame_name>')
def get_info(frame_name):
    return f"""
<img src="/img/{frame_name}"/>
<img src="/mask/{frame_name}"/>
"""

@app.route('/batch')
def batch():
    bsize = 128
    color = False
    gen = core.batch_generator(db, (128, 128), bsize, color=color, augment=True)
    imgs, bboxes = next(gen)
    rows = [
            np.concatenate([
                util.draw_bbox(img, bbox) for img, bbox in zip(imgs[a:a+16], bboxes[a:a+16])
            ], axis=1)
        for a in range(0, bsize, 16)
    ]
    return image_response(np.concatenate(rows)*255)


@app.route('/test')
def test():
    return 'bajs'

@app.route('/')
def framelist():
    return '<br/>'.join([f'<a href="/img/{i}">{i}</a>' for i in db.list_frame_names(in_subset=db.SET_TEST)])

@app.route('/random')
def random_frame():
    names = db.list_frame_names(in_subset=db.SET_TEST)
    name = random.choice(names)
    return get_frame(name)

@app.route('/worst')
def worst_frames():
    scoreboard = db.get_worst_predictions(100, args.model)
    return '<br/>'.join([
        f'<img src="/img/{name}"/ alt="{idx}">'
        for idx, (name, score) in enumerate(scoreboard)
    ])

@app.route('/synth')
def synth():
    import tensorflow.keras as K
    from mxaimbot import synthset
    model: K.Model = K.models.load_model(args.model)
    imgs, bboxes_true = next(synthset.synth_batch_generator(1))
    img = imgs[0] * 255
    true = util.scale_abnormalize_xywh(bboxes_true[0], (128, 128))
    pred = util.scale_abnormalize_xywh(model.predict(imgs)[0], (128, 128))
    img_drawn = cv2.circle(img, tuple(true[:2]), 5, (0, 0, 255), thickness=2)
    img_drawn = cv2.circle(img, tuple(pred[:2]), 5, (255, 0, 0), thickness=2)
    return image_response(img_drawn)



def run():
    app.run(host='0.0.0.0', port=args.port)

if __name__ == '__main__':
    run()
