import math
import random
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import numpy as np
import cv2 as cv
import os
from pathlib import Path

def clamp(x, minval, maxval):
    return max(min(x, maxval), minval)


def dist(p1, p2):
    return math.sqrt(sum([abs(a-b)**2 for a, b in zip(p1, p2)]))


def rotate(origin, point, angle):
    """
    Rotate a point counterclockwise by a given angle around a given origin.

    The angle should be given in radians.
    """
    ox, oy = origin
    px, py = point

    qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
    qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
    return qx, qy

def random_color(channels):
    return tuple([random.random() for i in range(channels)])



def gen_img(shape=(128, 128, 1)):
    # x = random.randint(0, shape[0])
    # y = random.randint(0, shape[1])
    radius = random.randint(10, 50)
    linewidth = radius//5 + random.randint(-1, 1)
    x, y = [
        int(clamp(np.random.normal(shape[t]/2, shape[t]/4), 0, shape[t]))
        for t in [0, 1]
    ]
    center = (shape[0]//2, shape[1]//2)
    bg_color = random_color(channels=shape[2])
    fg_color = random_color(channels=shape[2])
    img = np.ones(shape) * bg_color
    img = cv.ellipse(
        img,
        (x, y),
        (random.randint(0, radius), radius),
        0, 0, 360,
        fg_color,
        radius//4
    )
    img = cv.line(img, (x, y-radius-linewidth//2), (x, 0), fg_color, radius//5)
    
    # add crystal polygon
    polyrot = random.random() * 2 * math.pi
    corners = random.randint(3, 8)
    polypoints = np.array([
        list(rotate(
            (x, y),
            (x+(radius/2)*(1+2.5*random.random()), y),
            2 * math.pi * i / corners + polyrot
        ))
        for i in range(corners)
    ], dtype=np.int32).reshape(-1, 1, 2)
    
    # img = cv.fillConvexPoly(img, polypoints, fg_color)
    img = cv.polylines(img, [polypoints], True, fg_color, thickness=random.randint(1, linewidth))
    # calculate score
    centerpoint = Point(center)
    poly = Polygon([tuple(p[0]) for p in polypoints])
    centroid = poly.centroid.coords[0]
    return img, centroid


def synth_batch_generator(batch_size=64, shape=(128, 128, 1)):
    while True: # batch
        imgs = [None] * batch_size
        centroids = [None] * batch_size
        for idx in range(batch_size):
            img, centroid = gen_img(shape)
            imgs[idx] = img
            centroids[idx] = np.array([coord / 128 for coord in centroid] + [10/128, 10/128])
        imgs = np.stack(imgs)
        centroids = np.stack(centroids)
        yield imgs, centroids


if __name__ == '__main__':
    DST_DIR = Path('/tmp/synthdata')
    DST_DIR.mkdir(exist_ok=True)
    for idx in range(10):
        img, _ = gen_img()
        img = img * 255
        cv.imwrite(str(DST_DIR / f'synth_{idx}.png'), img)
