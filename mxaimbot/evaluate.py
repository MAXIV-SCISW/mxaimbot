"""
Script to help evaluate and compare the models that have made predictions on the current dataset.
"""
import argparse
import typing as T
import numpy as np
import plotille
from mxaimbot.core import Dataset
from mxaimbot import util, sanity_check

# pylint: disable=too-many-locals

def get_args():
    """
    get CLI args
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('dataset')
    parser.add_argument('--histogram', action='store_true')
    return parser.parse_args()


def insanity_rate(pred, imgs, **kwargs):
    totlen = len(pred)
    num_insane = 0
    for img, bbox in zip(imgs, pred):
        bbox = util.scale_abnormalize_xywh(bbox, (128, 128))
        sane, _ = sanity_check.sanity_check(img, tuple(bbox[:2]), 2)
        if not sane:
            num_insane += 1
    return num_insane / totlen


def intersection_over_union(real, pred, **kwargs):
    """
    accepts two normalized bboxes

    aaaaa
    aaiaa
    aaaaa
    >>> round(intersection_over_union((0.5, 0.5, 1, 1), (0.5, 0.5, 0.1, 0.1)), 2)
    0.01
    """
    rx, ry, rw, rh = real
    px, py, pw, ph = pred

    # calculating corners of the intersection
    ix1 = max(rx-rw/2, px-pw/2)
    iy1 = max(ry-rh/2, py-ph/2)
    ix2 = min(rx+rw/2, px+pw/2)
    iy2 = min(ry+rh/2, py+ph/2)

    if ix1 > ix2 or iy1 > iy2:
        # real and pred do not intersect
        return 0.0

    _, _, iw, ih = util.xyxy_to_xywh((ix1, iy1, ix2, iy2))
    intersection_area = iw*ih
    union_area = rw*rh + pw*ph - intersection_area
    if intersection_area > union_area:
        assert False
    return intersection_area / union_area


def distance(real, pred, **kwargs):
    return np.hypot(real[0]-pred[0], real[1]-pred[1])

def distance_y(real, pred, **kwargs):
    return abs(real[1] - pred[1])

def distance_x(real, pred, **kwargs):
    return abs(real[0] - pred[0])


def evaluate_all():
    """
    Look over all predictions to evaluate intersection over union
    print result
    """
    args = get_args()
    dset = Dataset(args.dataset)
    names = dset.list_frame_names(
        is_annotated=True,
        in_subset=dset.SET_TEST,
    )
    annotations = dset.get_annotations(names)
    imgs = dset.get_frame_images(names)


    # index predictions by model
    preds_per_model = dict()
    for name in names:
        preds_per_sample = dset.get_predictions(name)
        for model_name, bbox in preds_per_sample:
            if model_name not in preds_per_model:
                preds_per_model[model_name] = []
            preds_per_model[model_name].append(bbox)


    # add baseline
    preds_per_model['baseline_center'] = [(0.5, 0.5, 0, 0) for i in range(len(names))]

    # calculate scores per model
    evaluation = list()
    for model_name, bboxes in preds_per_model.items():
        print(f'\n{model_name}')
        if len(bboxes) != len(annotations):
            print(f'Skipping {model_name} since it has {len(bboxes)} predictions \
                    instead of {len(annotations)}, the number of annotations.')
            continue

        output = dict()
        for name, measure in [
                ('Distance (0-1)', distance),
                ('X-Distance (0-1)', distance_x),
                ('Y-Distance (0-1)', distance_y),
                ]:
            scores = [
                measure(real=real, pred=pred)
                for real, pred in zip(annotations, bboxes)
            ]
            median, std = np.median(scores), np.std(scores)
            output[name] = median
            # print(f'median {name}: {median}')
            # print(f'std {name}: {std}')
            if args.histogram:
                print(plotille.histogram(scores, bins=50))
        insanity = insanity_rate(bboxes, imgs)
        output['insanity'] = insanity
        evaluation.append((model_name, output))
    evaluation.sort(key=lambda x: x[1]['Distance (0-1)'], reverse=True)
    for model_name, scores in evaluation:
        print(f'\n{model_name}')
        print('-'*10)
        for key, val in scores.items():
            print(f'median {key}: {val}')

if __name__ == '__main__':
    evaluate_all()
