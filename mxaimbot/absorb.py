import argparse
from pathlib import Path
from datetime import datetime
import csv
import os
import re
import pytz
import cv2
from tqdm import tqdm
import numpy as np
from mxaimbot.h5db import H5Database
from mxaimbot import util, sieve

def get_args():
    """
    get commandline arguments
    """
    parser = argparse.ArgumentParser('mxaimbot-absorb')
    parser.add_argument('csv', type=Path)
    parser.add_argument('database', type=Path)
    # parser.add_argument('timesheet', type=Path)
    return parser.parse_args()


def is_bad_time(mtime) -> bool:
    """
    Determines if the given mtime is during a time when data is unlikely to be useful
    """
    wday = mtime.weekday()
    # Is it during a machine day or comissioning?
    if wday < 2 or wday == 2 and mtime.hour < 18:
        return True
    # Is it during summer shutdown?
    if mtime.month == 7:
        return True
    return False


def parse_timesheet(path) -> list:

    def parse_times(line):
        match = re.match(r'^start: (.*) end: (.*)$', line)
        startstr = match.group(1)
        endstr = match.group(2)
        start = datetime.fromisoformat(startstr)
        end = datetime.fromisoformat(endstr)
        return start, end

    with open(path, 'r') as fh:
        return [ parse_times(line) for line in fh.readlines() ]
            

def is_good_time(mtime: datetime, timesheet: list) -> bool:
    for start, end in timesheet:
        if start <= mtime <= end:
            return True
    return False


def frame_generator(csv_path, num_preexisting_frames=0, timesheet=None):
    with open(csv_path, 'r') as fh:
        reader = csv.reader(fh)
        csvdata = list(reader)
        print(f'skipping {num_preexisting_frames} already absorbed frames.')
        print(f'absorbing {len(csvdata) - num_preexisting_frames} new frames.')
        bad_count = 0
        good_count = 0
        for uuid, x, y, z in tqdm(csvdata):
            globmatches = list(csv_path.parent.glob(f'**/*_{uuid}.jpg'))
            if len(globmatches) != 1:
                print(f'weird, found wrong amount of matches for {uuid}, {globmatches}')
            imgpath = globmatches[0]
            mtime = datetime.fromtimestamp(os.path.getmtime(imgpath), tz=pytz.UTC)
            # if timesheet and not is_good_time(mtime, timesheet):
            if not is_bad_time(mtime):
                continue
            img = cv2.imread(str(imgpath), cv2.IMREAD_COLOR)
            if sieve.is_good(img, (int(float(x)), int(float(y)))):
                scaled_bbox = util.scale_normalize_xywh(
                    [float(x), float(y), 10.0, 10.0],
                    img.shape[:2]
                )
                data = dict(
                    img=img,
                    zoom=z,
                    time=mtime.timestamp(),
                    anno=np.array(scaled_bbox, dtype=np.float)
                )
                good_count += 1
                yield data
            else:
                bad_count += 1


def run(csv_path: Path, database_path: Path):
    db = H5Database(database_path)
    # timesheet = parse_timesheet(timesheet_path)
    # num_preexisting_frames = len(db.list_frame_names()) if os.path.isfile(database_path) else 0
    framegen = frame_generator(csv_path)
    db.add_frames(framegen)


if __name__ == '__main__':
    args = get_args()
    run(args.csv, args.database)
