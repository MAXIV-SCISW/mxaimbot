"Utility functions for mxaimbot"
from typing import Tuple
import numpy as np
import cv2

ImageSize = Tuple[float, float]

def xyxy_to_xywh(bbox: Tuple[int, int, int, int]) -> Tuple[int, int, int, int]:
    """
    Converts a rectangle defined by two corner points (x1, y1, x2, y2)
    to a rectangle defined by a centerpoint, a width, and a height (x, y, w, h)

    >>> xyxy_to_xywh((0,0,64,32))
    (32, 16, 64, 32)
    """
    x1, y1, x2, y2 = bbox
    x = (x1 + x2) // 2
    y = (y1 + y2) // 2
    w = abs(x1 - x2)
    h = abs(y1 - y2)
    return x, y, w, h

def xywh_to_xyxy(bbox: Tuple[int, int, int, int]) -> Tuple[int, int, int, int]:
    """
    Converts a rectangle defined by a centerpoint, a width, and a height (x, y, w, h)
    to a rectangle defined by two corner points (x1, y1, x2, y2)
    >>> xywh_to_xyxy((32, 16, 64, 32))
    (0, 0, 64, 32)
    """
    x, y, w, h = bbox
    return x-w//2, y-h//2, x+w//2, y+h//2

def scale_normalize_xywh(
        bbox: Tuple[int, int, int, int],
        img_size: Tuple[int, int],
    ) -> Tuple[float, float, float, float]:
    """
    >>> scale_normalize_xywh((8,16,32,16),(32,64))
    (0.125, 0.5, 0.5, 0.5)
    """

    img_height, img_width = img_size
    orig_x, orig_y, orig_w, orig_h = bbox

    x = orig_x / img_width
    y = orig_y / img_height
    w = orig_w / img_width
    h = orig_h / img_height

    return x, y, w, h


def scale_abnormalize_xywh(
        bbox: Tuple[float, float, float, float],
        img_shape: Tuple[int, int],
    ) -> Tuple[int, int, int, int]:
    """
    >>> scale_abnormalize_xywh((0.125, 0.5, 0.5, 0.5),(32,64))
    (8, 16, 32, 16)
    """
    img_height, img_width = img_shape
    x, y, w, h = scale_normalize_xywh(bbox, (1/img_height, 1/img_width))
    return int(x), int(y), int(w), int(h)


def xyxy_to_scaled_xywh(
        bbox: Tuple[int, int, int, int],
        img_size: Tuple[int, int],
    ) -> Tuple[float, float, float, float]:
    """
    x1,y1,x2,y2 bounding box to scaled x,y,width,height bounding box
    >>> xyxy_to_scaled_xywh((4,4,12,12),(16,16))
    (0.5, 0.5, 0.5, 0.5)
    """
    return scale_normalize_xywh(xyxy_to_xywh(bbox), img_size)


def scaled_xywh_to_xyxy(
        bbox: Tuple[float, float, float, float],
        img_size: Tuple[int, int],
    ) -> Tuple[int, int, int, int]:
    """
    Scaled x,y,width,height bounding box to x1,y1,x2,y2 bounding box
    >>> scaled_xywh_to_xyxy((0.5, 0.5, 0.5, 0.5),(16,16))
    (4, 4, 12, 12)
    """
    return xywh_to_xyxy(scale_abnormalize_xywh(bbox, img_size))


def square_crop(img: np.ndarray) -> np.ndarray:
    """
    Returns a cropped version of the given image.
    Assumes the image is wider than it is tall.
    if img has shape (a, b), the returned image has shape (a, a)
    the returned image is centered on the same point as the original image
    """
    sqside = img.shape[0]
    hcenter = img.shape[1] // 2
    return img[:, hcenter-sqside//2:hcenter+sqside//2]



def decode_jpg(jpg: bytes):
    m = np.frombuffer(jpg, dtype=np.uint8)
    return cv2.imdecode(m, cv2.IMREAD_COLOR)



def transform_img(img: np.ndarray, final_imsize: Tuple[int, int]):
    """
    parameters:
    img: BGR color image as produced by cv2.imread
    final_imsize: the shape an image should have after the transformation.
    """

    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img = cv2.resize(img, final_imsize[:2])
    img = img.reshape(final_imsize)
    return img

def draw_bbox(img: np.ndarray, bbox: Tuple[float, float, float, float]) -> np.ndarray:
    xywh = scale_abnormalize_xywh(bbox, img.shape[:2])
    xyxy = xywh_to_xyxy(xywh)
    ret = img
    ret = cv2.rectangle(ret, (xyxy[0], xyxy[1]), (xyxy[2], xyxy[3]), (0, 255, 0))
    ret = cv2.circle(ret, xywh[:2], 2, (0, 255, 0))
    return ret
