"HTTP server for MXAimbot"
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path
import typing as T
import sys
from flask import Flask, request, current_app, g
import cv2
import numpy as np
import keras as K
from mxaimbot import core
from mxaimbot import util
from mxaimbot.sanity_check import sanity_check, sane_xcenter_from_img, mask_out_bg

app = Flask(__name__)
model: K.Sequential = None

import time


def save_bbox_visual(img: np.ndarray, bbox: T.Tuple[int, int, int, int], path: T.Union[Path, str]):
    "Save to disk the image (img) to (path) with (bbox) drawn on it in blue"
    x1, y1, x2, y2 = util.xywh_to_xyxy(bbox)
    print(x1, y1, x2, y2)
    visual = cv2.rectangle(img, (x1, y1), (x2, y2), 255)
    center = bbox[:2]
    visual = cv2.circle(img=visual, center=center, radius=4, color=255)
    Path(path).parent.mkdir(parents=True, exist_ok=True)
    cv2.imwrite(str(path), visual)


def get_args():
    "Get commandline arguments with argparse"
    argp = ArgumentParser()
    argp.add_argument('model', help='path to model file (hdf5)', type=str)
    argp.add_argument('--port', type=int, default=7777)
    argp.add_argument(
        '--save-visuals-to',
        help='For debugging purposes: a directory to which to save all incoming\
                images with their bounding boxes drawn into them.'
    )
    argp.add_argument(
        '--debug',
        help='Run flask server in debug mode. (broken)',
        action='store_true'
    )
    argp.add_argument('--no-tta', action='store_true', help='don\'t use test time augmentation')
    return argp.parse_args()


def decode_jpg(jpg: bytes) -> np.ndarray:
    "Turn a jpg bytestring into a numpy array, assumes a color image"
    m = np.frombuffer(jpg, dtype=np.uint8)
    return cv2.imdecode(m, cv2.IMREAD_COLOR)



@app.route('/predict', methods=['POST'])
def route_predict():
    """
    Reads a single jpg image from the POST data
    Response is a json that looks like {bbox: [512, 336, 52, 106]}
    """
    timebefore = time.time()

    model_path: str = current_app.config['MODEL']
    global model

    # Get image from request
    jpg = request.get_data()
    img = decode_jpg(jpg)
    if img is None:
        print('The data recieved was not recognized as an image.', file=sys.stderr)
        return 'What you sent was not recognized as an image.', 400
    norm_img = core.transform_img(img, model.input_shape[1:3])

    batch_imgs = [norm_img]
    # test time augmentation
    if current_app.config['USE_TTA']:
        flipped_img = np.flip(norm_img, axis=1)
        batch_imgs.append(flipped_img)

    batch = np.stack([
        img.reshape(model.input_shape[1:])
        for img in batch_imgs
    ])

    # Predict bounding box
    preds = model.predict(batch)
    preds = [
        [float(i) for i in bbox]
        for bbox in preds
    ]
    if current_app.config['USE_TTA']:
        norm_bbox = core.merge_flipped_norm_bbox(preds[0], preds[1])
    else:
        norm_bbox = preds[0]

    # Turn the bbox coordinates and sizes into pixels
    bbox = util.scale_abnormalize_xywh(norm_bbox, img.shape[:2])

    # save visual of predicted bbox for debugging purposes
    if current_app.config['VISUALS_DIR'] is not None:
        visdir = Path(current_app.config['VISUALS_DIR'])
        visdir.mkdir(exist_ok=True)
        filename = 'bbox_{}_{}.jpg'.format(
            Path(model_path).stem,
            datetime.now().strftime('%Y%m%d%H%M%S')
        )
        save_bbox_visual(
            img,
            bbox,
            Path(current_app.config['VISUALS_DIR']) / filename
        )
        (visdir / 'raw').mkdir(exist_ok=True)
        cv2.imwrite(str( visdir / 'raw' / filename), img)

    timeafter = time.time()
    print('responsetime:', timeafter-timebefore)
    return dict(bbox=bbox)

@app.route('/sanitycheck', methods=['POST'])
def route_sanity_check():
    """
    Reads a single jpg from the POST data.
    Tries to determine if the image is centered on the background or some object.
    Returns True if centered on something other than the background.
    """
    jpg = request.get_data()
    img = decode_jpg(jpg)
    passed, mask = sanity_check(img, 2)
    if current_app.config['VISUALS_DIR'] is not None:
        save_dir = Path(current_app.config['VISUALS_DIR'])
        filename = 'sanity_{}.png'.format(
            datetime.now().strftime('%Y%m%d%H%M%S')
        )
        cv2.imwrite(str(save_dir / filename), mask)

    return dict(passed=passed)


@app.route('/status', methods=['GET'])
def route_status():
    """
    Used to check that the server is online and gives some basic info about the running server.
    """
    return dict(
        model=app.config['MODEL'],
        use_tta=app.config['USE_TTA']
    )


def main():
    args = get_args()
    app.config['VISUALS_DIR'] = args.save_visuals_to
    app.config['PORT'] = args.port
    app.config['MODEL'] = args.model
    app.config['DEBUG'] = args.debug
    app.config['USE_TTA'] = not args.no_tta
    global model
    try:
        model = K.models.load_model(app.config['MODEL'])
    except ImportError:
        print('Model could not be loaded because h5py is not available to be imported.',
              file=sys.stderr)
        sys.exit(1)
    except IOError:
        print(f'The saved model file "{app.config["MODEL"]}" is invalid and cannot be loaded :(',
              file=sys.stderr)
        sys.exit(1)

    app.run(
        host='0.0.0.0',
        port=app.config['PORT'],
        threaded=False,
        debug=app.config['DEBUG'],
    )


if __name__ == '__main__':
    main()
