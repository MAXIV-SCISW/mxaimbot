"""
Core functionality for mxaimbot.
"""
from typing import Tuple, Union, Collection
from random import random
import cv2
import numpy as np
import imgaug as ia
from mxaimbot.h5db import H5Database as Dataset
from mxaimbot.sanity_check import flatten_background
from mxaimbot import util

transform_cache = dict()

def transform_img(
        img: np.ndarray,
        final_imsize: Tuple[int, int],
        color=False,
        name=None,
        flat_bg=False
    ):
    """
    parameters:
    img: BGR color image as produced by cv2.imread
    final_imsize: the shape an image should have after the transformation.
    """

    if name is not None and name in transform_cache:
        return transform_cache[name]
    img = cv2.resize(img, final_imsize[:2])
    if not color:
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img = img.reshape(final_imsize)
    if flat_bg:
        img = flatten_background(img)
    img = img / 255
    if name is not None:
        transform_cache[name] = img
    return img


def merge_flipped_norm_bbox(real, flipped):
    """
    >>> merge_flipped_norm_bbox([1.25,1,1,1], [0.25,1.5,1.5,1.5])
    array([1.  , 1.25, 1.25, 1.25])
    """
    unflipped = (1 - flipped[0], *flipped[1:])
    return np.average([real, unflipped], axis=0)


def batch_generator(
        h5file: Union[Dataset, str],
        output_imsize: Tuple[int, int],
        batch_size: int = 64,
        in_subset: Union[int, Collection[int]] = None,
        flip_aug_frequency: float = 0.0,
        augment: bool = False,
        aug_flip_p=.5,
        aug_rot90_k=(0, 3),
        color: bool = False,
        ):
    """
    Generates batches of images and bboxes from the given mxaimbot structured hdf5 file.
    in_subset is set with the SET constants in h5db.H5Database or a list of them
    flip_aug_frequency is how often the image and bbox will be randomly
    horizontally flipped. This should be a float in [0,1] and is meant for data augmentation.
    so setting 0.0 will never flip and 1.0 will always flip.
    """
    db = h5file if isinstance(h5file, Dataset) else Dataset(h5file)
    names = db.list_frame_names(is_annotated=True, in_subset=in_subset)
    names.sort()
    start_idx = 0
    print("AUGMENT IS", augment)
    while True:
        batch_names = names[start_idx:start_idx+batch_size]
        images = db.get_frame_images(batch_names)
        labels = db.get_annotations(batch_names)
        for idx, (name, img, bbox) in enumerate(zip(batch_names, images, labels)):
            img = transform_img(img, final_imsize=output_imsize, color=color, name=name)
            # augment data
            if augment:
                seq = ia.augmenters.Sequential([
                    ia.augmenters.Fliplr(p=aug_flip_p),
                    ia.augmenters.Rot90(k=aug_rot90_k),
                    ])
                xyxy = util.scaled_xywh_to_xyxy(bbox, img.shape[:2])
                img, imgaug_bbox = seq.augment(
                    image=img,
                    bounding_boxes=ia.augmentables.bbs.BoundingBox(*xyxy)
                )
                bbox = util.xyxy_to_scaled_xywh((
                    imgaug_bbox.x1,
                    imgaug_bbox.y1,
                    imgaug_bbox.x2,
                    imgaug_bbox.y2,
                    ), img.shape[:2])
            images[idx] = img
            labels[idx] = bbox
        img_mat = np.stack(images)
        label_mat = np.array(labels)
        yield (img_mat, label_mat)
        start_idx = (start_idx + batch_size) % len(names)
