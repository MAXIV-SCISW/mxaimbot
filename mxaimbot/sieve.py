from pathlib import Path
import sys
import cv2
import numpy as np
import csv
from mxaimbot import sanity_check

def bright_border_ratio(img: np.ndarray) -> float:
    darkness_threshold = 30
    height, width = img.shape
    border_pixels = np.concatenate([img[0, :], img[height-1, :], img[:, 0], img[:, width-1]])
    is_bright = (border_pixels > darkness_threshold)
    bright_ratio = is_bright.sum() / len(is_bright)
    return bright_ratio


def max_sharpness(img: np.ndarray, ksize=13) -> float:
    lap = cv2.Laplacian(img, cv2.CV_64F, ksize=ksize)
    max_sharp = np.abs(lap).max() ** (1 / ksize) # normalize values
    return max_sharp


def score_all(filenames: list, score_function: callable):
    table = [None]*len(filenames)
    for idx, fn in enumerate(filenames):
        img = cv2.imread(str(fn))
        if img is None:
            print('could not read filepath', fn)
            sys.exit(1)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        score = score_function(gray)
        table[idx] = (fn, score)
    table.sort(key=lambda x: x[1])
    return table


def is_good(img: np.ndarray, point):
    border_light_thresh = .5
    sharpness_thresh = 3.7
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    is_bright = bright_border_ratio(gray_img) > border_light_thresh
    return is_bright \
            and max_sharpness(gray_img) > sharpness_thresh \
            and sanity_check.sanity_check(img, point, 3)[0]


if __name__ == '__main__':
    filenames = list(Path(sys.argv[1]).glob('*.jpg'))
    for fn in filenames:
        img = cv2.imread(str(fn))
        if img is None:
            print('could not read filepath', fn)
            sys.exit(1)
