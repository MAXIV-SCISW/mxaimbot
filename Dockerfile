FROM ubuntu:latest
FROM python:3.7
MAINTAINER Isak Lindhé isak.lindhe@maxiv.lu.se

# opencv vult!
RUN apt-get update
RUN apt-get install 'ffmpeg'\
    'libsm6'\ 
    'libxext6'  -y

COPY . /app
WORKDIR /app
RUN pip install /app

EXPOSE 7777

# CMD bash
CMD mxaimbot-server --port 7777 /app/trained_models/mid_frag_aug_003.h5
