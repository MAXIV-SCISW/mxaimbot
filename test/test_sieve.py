import cv2
from mxaimbot import sieve


def test_sieve():
    black_bg = cv2.imread("") # TODO
    low_contrast = cv2.imread("") # TODO

    ideal = cv2.imread("") # TODO
    ok = cv2.imread("") # TODO

    assert not sieve.is_good(black_bg)
    assert not sieve.is_good(low_contrast)
    assert sieve.is_good(ideal)
    assert sieve.is_good(ok)
