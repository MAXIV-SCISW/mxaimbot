import argparse
from pathlib import Path
from mxaimbot import absorb
from mxaimbot.h5db import H5Database

def test_absorb():
    srcpath = Path('test/data/mock_camdump_meta.csv')
    dstpath = Path('test/output/absorbed.h5')
    try:
        dstpath.unlink()
    except FileNotFoundError:
        pass
    absorb.run(srcpath, dstpath)
    assert dstpath.is_file()
    db = H5Database(dstpath)
    fnames = db.list_frame_names()
    assert len(fnames) == len(open(srcpath, 'r').readlines())
