from pathlib import Path
import logging
from tqdm import tqdm
from mxaimbot.pred_server import app

log = logging.getLogger('unittests')
log.setLevel(logging.DEBUG)

app.config['MODEL'] = './storage/models/best.h5'
app.config['USE_TTA'] = True
app.config['VISUALS_DIR'] = '/tmp/utest_visuals'

def get_test_images():
    imgdir = Path('./storage/test_images/')
    return list(imgdir.glob('*.jpg'))


def test_predict():
    api = app.test_client()
    imgs = get_test_images()
    for imgpath in imgs[:10]:
        with open(imgpath, 'rb') as jpg:
            res = api.post('/predict', data=jpg.read())
            assert res
            assert res.status_code == 200
            json = res.json
            log.debug(json)
            assert json
            assert 'bbox' in json


def test_sanity_check():
    api = app.test_client()
    imgs = get_test_images()
    for imgpath in imgs[:10]:
        with open(imgpath, 'rb') as jpg:
            res = api.post('/sanitycheck', data=jpg.read())
            assert res
            assert res.status_code == 200
            json = res.json
            log.debug(json)
            assert json
            assert 'passed' in json

def test_status():
    api = app.test_client()
    res = api.get('/status')
    assert res.status_code == 200

