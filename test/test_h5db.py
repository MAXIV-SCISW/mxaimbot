import os
import numpy as np
import pytest
from mxaimbot.h5db import H5Database

# pylint: disable=redefined-outer-name,missing-function-docstring

def clean_db():
    try:
        os.unlink('/tmp/testdb.h5')
    except FileNotFoundError:
        pass
    return H5Database('/tmp/testdb.h5')

@pytest.fixture
def db() -> H5Database:
    ret = clean_db()
    ret.add_frame(np.zeros((5, 5)))
    ret.add_frame(np.ones((5, 5)))
    return ret


def test_frame(db: H5Database):
    before = len(db.list_frame_names())
    content = np.ones((10, 10))
    name = db.add_frame(content)
    after = len(db.list_frame_names())
    assert isinstance(name, str)
    assert 'FRAME_' in name
    assert after == before + 1
    assert (db.get_frame_image(name) == content).all()


def test_annotation(db: H5Database):
    fname = db.list_frame_names()[0]
    anno = (1.0, 2.0, 3.0, 4.0)
    db.add_annotation(fname, anno, annotation_name='berp')
    assert db.get_annotation(fname, annotation_name='berp') == anno
    assert len(db.get_annotations([fname], annotation_name='berp')) == 1
