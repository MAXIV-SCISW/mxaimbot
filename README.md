# Running with a trained model

	python3 pred_server.py /path/to/model.h5

Remember that problems can occur if the model was trained with later versions of keras/tensorflow than is in your environment.

# API routes

# `/predict`
expects a `POST` request whose data is a `jpg` encoded image.
The response is a json that looks like this:

	{ bbox: [x, y, width, height] }

# `/sanitycheck`
expects a `POST` request whose data is a `jpg` encoded image.
The response is a json that looks like this:

	{passed: True}

The sanity check confirms that the center of the given image does not contain the background.
In the future the size of this checked area will be adjustable with a parameter.

## Example

	import requests

	res = requests.post(
		'http://localhost:7777/predict',
		data=open('./sample.jpg','rb').read()
	)
	print(res.json()['bbox'])

